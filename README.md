<p align="center"><a href="https://envelapp.larapp.fr/" target="_blank"><img src="https://envelapp.larapp.fr/img/logo_envelapp.svg" width="400" alt="Envelapp Logo"></a></p>

<p align="center">
<a href="https://gitlab.com/larapp-officiel/envelapp/activity"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/larapp-officiel/envelapp"><img src="https://img.shields.io/packagist/dt/larapp-officiel/envelapp" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/larapp-officiel/envelapp"><img src="https://img.shields.io/packagist/v/larapp-officiel/envelapp" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/larapp-officiel/envelapp"><img src="https://img.shields.io/packagist/l/laravel/laravel" alt="License"></a>
</p>

## About Envelapp
Envelapp is a set of powerful tools that offers you the best environment for creating your web applications. We believe that development must be an enjoyable and creative experience to be truly fulfilling. Envelapp simplifies development by providing you with a complete environment with all the services you need.

- [Liste des services](https://larapp.fr/docs/envelapp).

Envelapp is accessible, powerful, and provides tools required for large, robust applications.

## Learning Envelapp
Envelapp has the most extensive and thorough [documentation](https://envelapp.larapp.fr//docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

## Contributing
Thank you for considering contributing to the Envelapp! The contribution guide can be found in the [Laravel documentation](https://envelapp.larapp.fr//docs/contributions).

## Code of Conduct
In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://envelapp.larapp.fr//docs/contributions#code-of-conduct).

## Security Vulnerabilities
If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License
Envelapp is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT). 

## roadmap 
- update technology
- intégration Envelapp & Larapp for Kubernetes (https://helm.sh/docs/intro/quickstart/)