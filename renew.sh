#!/bin/sh

RED='\033[0;31m'
NC='\033[0m' # No Color

printf "

  ███████ ███    ██ ██    ██ ███████ ██       █████  ██████  ██████
  ██      ████   ██ ██    ██ ██      ██      ██   ██ ██   ██ ██   ██
  █████   ██ ██  ██ ██    ██ █████   ██      ███████ ██████  ██████
  ██      ██  ██ ██  ██  ██  ██      ██      ██   ██ ██      ██
  ███████ ██   ████   ████   ███████ ███████ ██   ██ ██      ██

 "
printf "${RED} *************** ----- DOCKER exec ----- *************** ${NC} \n"

cd /home/administrator/envelapp
docker compose run --rm certbot renew --force-renewal
./sail restart nginx