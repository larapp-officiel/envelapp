#!/bin/bash

# Function to export variables from .env file
export_env_variables() {
  if [ -f /etc/nginx/.env ]; then
    export $(grep -vE "^(#.*|\s*)$" /etc/nginx/.env)
  fi
}

# Generate NGINX configuration files
generate_nginx_config() {
  # Substitute variables in app.conf.template and save as default.conf
  #envsubst '${VIRTUAL_HOST} ${PUBLIC_DIRECTORY}' < /etc/nginx/conf.d/app.conf.template > /etc/nginx/conf.d/default.conf

  # Check and create applocalhost.conf based on the environment variables
  if [[ "$LOCAL_OCTANE" = true && "$LOCAL" = true ]]; then
    envsubst '${LOCAL_FOLDER} ${LOCAL_INDEX} ${PUBLIC_DIRECTORY}' < /etc/nginx/stubs/octane.stub > /etc/nginx/conf.d/applocalhost.conf
  elif [[ "$LOCAL" = true ]]; then
    envsubst '${LOCAL_FOLDER} ${LOCAL_INDEX} ${PUBLIC_DIRECTORY} ${PHP_EXPOSE}' < /etc/nginx/stubs/php.stub > /etc/nginx/conf.d/applocalhost.conf
  fi
}

# Export environment variables from .env file
export_env_variables

# Generate NGINX configuration files
generate_nginx_config

# Execute the provided command
exec "$@"
