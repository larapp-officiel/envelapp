#!/bin/bash

# Export variables from .env file (if exists)
export_env_variables() {
  if [ -f /usr/local/etc/php/.env ]; then
    export $(grep -vE "^(#.*|\s*)$" /usr/local/etc/php/.env)
  fi
}

# Handle xDebug configuration based on LOCAL and LOCAL_XDEBUG variables
configure_xdebug() {
  if [[  "$LOCAL_XDEBUG" = 'true' && -f "/usr/local/etc/php/docker-php-ext-xdebug.ini" ]]; then
    mv /usr/local/etc/php/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
    echo "xDebug is enabled"
  elif [[  "$LOCAL_XDEBUG" = 'false' && -f "/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini" ]]; then
    mv /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini /usr/local/etc/php/docker-php-ext-xdebug.ini
    echo "xDebug is disabled"
  fi
}

# Start appropriate supervisor process based on LOCAL_OCTANE and LOCAL variables
start_supervisor() {
  if [[ "$LOCAL_OCTANE" = true && "$LOCAL" = true ]]; then
    exec /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.octane.conf
  elif [[ "$LOCAL" = true ]]; then
    exec /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.php.conf
  else
    exec /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
  fi
}

# Export environment variables from .env file
export_env_variables

# Configure xDebug
configure_xdebug

# Start the appropriate supervisor process
start_supervisor

# Execute the provided command
exec "$@"
